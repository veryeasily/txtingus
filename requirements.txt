tensorflow==1.15.4
keras==2.2.5
bottle==0.12.19
textgenrnn==1.3.2
h5py<3.0.0
