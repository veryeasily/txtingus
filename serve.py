from textgenrnn import textgenrnn

def get_model_api():
    textgen = textgenrnn()

    def model_api(input_data):
        print(input_data)
        return textgen.generate(prefix=input_data,return_as_list=True)[0]

    return model_api
