FROM python:3.6.6-stretch
ENV APP_DIR="/app/txtingus"
ENV FLASK_APP="app.py"

RUN apt-get update && apt-get install -y wget curl build-essential

RUN mkdir /root/.keras
ADD [".keras/keras.json", "/root/.keras/"]

WORKDIR $APP_DIR
ADD ["requirements.txt", "$APP_DIR/requirements.txt"]
RUN pip install --upgrade pip && pip install --no-cache-dir -r requirements.txt
CMD python app.py
