from bottle import route, run
from textgenrnn import textgenrnn
import urllib.parse

textgen = textgenrnn()

@route('/<text>')
def hello(text):
    text = urllib.parse.unquote(text)
    result = textgen.generate(prefix=f"{text} ",
                              return_as_list=True,
                              max_gen_length=200)[0]
    temp = result[len(text)+1:]
    return temp

run(host='0.0.0.0', port=5000)
